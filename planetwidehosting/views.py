from django.shortcuts import render, render_to_response
from django.contrib.auth import login, logout, authenticate
from django.http import HttpResponseRedirect
from .forms import RegistrationForm, SystemVariableForm
from django.contrib import auth, messages
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.template import RequestContext
from .models import SystemVariable



def home(request):
	return render(request, 'home.html', {})


def user_login(request):
	if request.method == 'POST':
		
		username = request.POST.get('username', '')
		password = request.POST.get('password', '')
		user = authenticate(username=username, password=password)

		if user:
			if user.is_active:
				login(request, user)
				return HttpResponseRedirect("/")
	return render(request, 'base/user_login.html', {})


def user_logout(request):
	logout(request)
	return HttpResponseRedirect("/")


def user_register(request):
	if request.method == 'POST':
		registration_form = RegistrationForm(request.POST)
		if registration_form.is_valid():
			registration_form.save()
			messages.add_message(request, messages.INFO, 'User is registered successfully. Please login now.')
			return HttpResponseRedirect("/")
	return render(request, 'base/user_register.html', {})


@login_required
@csrf_exempt
def sys_variable(request):
    try:
        sv = SystemVariable.objects.get(id=1)
    except:
        sv = None
    # import pdb; pdb.set_trace()
    if request.method == 'POST':
        sv_form = SystemVariableForm(request.POST, instance=sv)
        if sv_form.is_valid():
            sv_form.save()
            return HttpResponseRedirect("/")
        else:
            return render(request, "base/sys-variable.html", 
                {'form': sv_form,'sv': sv})
    else:
        sv_form = SystemVariableForm(instance=sv)
        return render_to_response("base/sys-variable.html", 
            {'form': sv_form, 'sv': sv}, 
            context_instance=RequestContext(request))