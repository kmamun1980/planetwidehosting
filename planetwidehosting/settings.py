# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'z26cd&-4z3jzop)q63o5=a@mwwtpazgyq%%(uqi!fmqxpwfqv5'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'south',
    'compressor',
    'haystack',

    'planetwidehosting',
    'contacts',
    'inventory',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'planetwidehosting.urls'


WSGI_APPLICATION = 'planetwidehosting.django.wsgi'

ADMINS = (
    ('mamun', 'mamun1980@gmail.com'),
    ('pkudla', 'paul@scom.ca'),
)

MANAGERS = ADMINS

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'planet_billing',                      
        'USER': 'pgsql',                      
        'PASSWORD': 'Scom411400',             
        'HOST': '10.221.0.41',                
        'PORT': '5433',                   
    }
}
# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/Toronto'


USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/statics/'

STATIC_ROOT = '/usr/home/www/admin.planetwidehosting.com/planetwidehosting/statics/'

# STATICFILES_DIRS = '/usr/home/www/admin.planetwidehosting.com/planetwidehosting/statics/'

MEDIA_ROOT = '/usr/home/www/admin.planetwidehosting.com/planetwidehosting/media/'

MEDIA_URL = '/media/'


TEMPLATE_DIRS = (       
    os.path.join(BASE_DIR, 'templates/'),
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

COMPRESS_PRECOMPILERS = (
   ('text/less', 'lessc {infile} {outfile}'),
)


HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        # 'ENGINE': 'planetwidehosting.search_backend.ConfigurableElasticSearchEngine',
        # 'ENGINE': 'elasticstack.backends.ConfigurableElasticSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INDEX_NAME': 'contact',
    },
    'pwh_contact': {
        # 'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'ENGINE': 'planetwidehosting.search_backend.ConfigurableElasticSearchEngine',
        # 'ENGINE': 'elasticstack.backends.ConfigurableElasticSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INDEX_NAME': 'contact',
        "INDEX": "not_analyzed",
        'EXCLUDED_INDEXES': ['inventory.search_indexes.ItemIndex',],
    },
    'pwh_inventory': {
        # 'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'ENGINE': 'planetwidehosting.search_backend.ConfigurableElasticSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INDEX_NAME': 'inventory',
        "INDEX": "not_analyzed",
        'EXCLUDED_INDEXES': ['contacts.search_indexes.ContactIndex',],
    },
}

HAYSTACK_DEFAULT_OPERATOR = 'AND'

HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'

HAYSTACK_ROUTERS = ['inventory.routers.MasterRouter', 'haystack.routers.DefaultRouter',]

# =========== local settings ===============
try:
    from local_settings import *
except ImportError:
    pass