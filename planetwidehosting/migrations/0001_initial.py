# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SystemVariable'
        db.create_table(u'planetwidehosting_systemvariable', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('next_item_number', self.gf('django.db.models.fields.IntegerField')(null=True)),
        ))
        db.send_create_signal(u'planetwidehosting', ['SystemVariable'])


    def backwards(self, orm):
        # Deleting model 'SystemVariable'
        db.delete_table(u'planetwidehosting_systemvariable')


    models = {
        u'planetwidehosting.systemvariable': {
            'Meta': {'object_name': 'SystemVariable'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'next_item_number': ('django.db.models.fields.IntegerField', [], {'null': 'True'})
        }
    }

    complete_apps = ['planetwidehosting']