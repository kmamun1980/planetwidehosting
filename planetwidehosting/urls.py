from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'planetwidehosting.views.home', name='home'),
    url(r'^user/login/$', 'planetwidehosting.views.user_login', name='user_login'),
    url(r'^user/logout/$', 'planetwidehosting.views.user_logout', name='user_logout'),
    url(r'^user/register/$', 'planetwidehosting.views.user_register', name='user_register'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^system-variable/$', 'planetwidehosting.views.sys_variable', name='sys-variable'),

    url(r'^contacts/', include('contacts.urls')),
    url(r'^inventory/', include('inventory.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
	    {'document_root', settings.STATIC_ROOT}
	  ),
)
