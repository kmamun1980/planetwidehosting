from django import forms
from django.contrib.auth.models import User
from planetwidehosting.models import SystemVariable

class RegistrationForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ("username", "email", "first_name", "last_name", "password")

	def save(self, commit=True):
		user = super(RegistrationForm, self).save(commit=False)
		user.set_password(self.cleaned_data['password'])
		user.save()
		
		return user


class SystemVariableForm(forms.ModelForm):
	class Meta:
		model = SystemVariable