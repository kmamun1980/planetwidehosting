from django.db import models

class SystemVariable(models.Model):
	next_item_number = models.IntegerField(null=True, blank=False)
	
	def __unicode__(self):
		return str(self.id)

	class Meta:
		verbose_name = "System variable"
		verbose_name_plural = "System variables"

		permissions = (
    		('view_system_variable', 'Can View System Variable'),
    	)