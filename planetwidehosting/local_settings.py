import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
DEBUG = True

STATIC_ROOT = BASE_DIR + '/../statics/'

STATICFILES_DIRS = (BASE_DIR + '/statics/',)

MEDIA_ROOT = BASE_DIR + '/media/'

WSGI_APPLICATION = 'planetwidehosting.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'planet',                      
        'USER': 'postgres',                      
        'PASSWORD': 'qweqwe',             
        'HOST': 'localhost',                
                        
    }
}